#include "InputMemmoryFile.H"
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>

InputMemoryFile::InputMemoryFile(const char *pathname):
	data_(0),
	size_(0),
	file_handle_(-1)
{

	file_handle_ = ::open(pathname, O_RDONLY);
	if (file_handle_ == -1) return;
	struct stat sbuf;
	if (::fstat(file_handle_, &sbuf) == -1) return;
	data_ = static_cast<const char*>(::mmap(
                                 	0, sbuf.st_size,
					PROT_READ, MAP_SHARED,
					file_handle_, 0));
	if (data_ == MAP_FAILED) data_ = 0;
	else size_ = sbuf.st_size;
}

InputMemoryFile::~InputMemoryFile() {
	::munmap(const_cast<char*>(data_), size_);
	::close(file_handle_);
}
