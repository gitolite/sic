#include "sicccPersister.H"

int
SicccPersister::write_sicccfile(const Sicccobject& so)
{
    json_t *new_json = get_json_from_sicccobject(so);
    if (json_object_update(root, new_json) == 0)
        std::cout << "[DEBUG] update root json successfully" << std::endl;
    else
        std::cerr << "Error update root failed" << std::endl;
    assert(root);
    int ret = write_sicccfile();
    return ret;
}

int
SicccPersister::write_sicccfile()
{
    assert(!json_file_name.empty());
    std::string store_str = merouter.get_json_path() + "/" + beautify_filename();
    assert(root);
    int ret = json_dump_file(root, store_str.c_str(), JSON_INDENT(4));
    return ret;
}

Sicccobject
SicccPersister::read_json(const std::string &s)
{
    root = load_json(s);
    return Sicccobject(root);
}

