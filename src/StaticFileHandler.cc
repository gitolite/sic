#include "StaticFileHandler.H"

#include <iostream>


	std::string
StaticFileHandler::_cleanpath (const char * const path)
{
	//adds no security at all
	return "static/"+std::string(path);
}

	int 
StaticFileHandler::answer_pathreq(const char * const path,
		struct mg_connection *conn)
{
	FILE *fp ;
	char buf[1024];
	size_t n = 0;

	fp = fopen(_cleanpath(path).c_str(), "r");

	if (fp != NULL) {
		std::cerr << "outfile" << std::endl;
		while ((n =fread(buf,1,sizeof(buf),fp)) > 0){
			mg_send_data(conn,buf,n);
		}
		fclose(fp);
		return MG_TRUE;
	}
	return MG_FALSE;
}

