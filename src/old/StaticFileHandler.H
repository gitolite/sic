#pragma once
#include <mongoose.h>

class StaticFileHandler
{
	public:
	virtual int answer_pathreq(const char * const path,struct mg_connection *conn)=0;
};

