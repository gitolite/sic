
#include <string>
#include "UltraSimpleStaticFileHandler.H"
#include <mongoose.h>

static const char * hello_world_html = u8R"HERE(
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="generator" content="pandoc">
  <title></title>
</head>
<body>
<h1 id="sharing-is-caring">Sharing is caring</h1>
<p>Hello World!</p>
<h2 id="it-works">It works</h2>
<!--«»«»«»«»«»«»«»«»«»«»«»«»«»«»«»«»«»«»«»««»«»«»«»«»«»«»«»«»«»«»«»-->
<p>It seems like a working page has been served</p>
</body>
</html>)HERE";

#include <iostream>

UltraSimpleStaticFileHandler::UltraSimpleStaticFileHandler()
{
	std::cout << "hallo ausm construktor von USSFH [" << this <<  "] "
		<< std::endl;
}

UltraSimpleStaticFileHandler::~UltraSimpleStaticFileHandler(){
	std::cout << "Arrrr aus destruktor! und (∀Gute) zum Pinguintag!!" << std::endl;
}
	
int UltraSimpleStaticFileHandler::answer_pathreq(const char * const path __attribute__((unused)), 
                                             struct mg_connection *conn)
{
    mg_printf(conn,  "HTTP/1.0 200 OK\r\n"
            "Content-Length: %lu\r\n"
            "Content-Type: text/html\r\n\r\n%s",
            sizeof(hello_world_html),
            hello_world_html);
    return 0;
}

