
#include <cstring>
#include <cstdlib>
#include <iostream>
#include "Foohash.H"
#include "Routerin.H"
#include "DynamicObjects.H"

using std::strcmp;
using std::cout;
using std::endl;

//put this in class ?? Util??
//FIXME:
static const std::string std_html_header= u8R"HUENERBLUBBE(<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Sharing is Caring</title>
    <link rel="stylesheet" type="text/css" href="static/style.css">
  </head>
)HUENERBLUBBE" ;


static const std::string std_mainpage= u8R"HASENBLUBBE(
  <body>
    <div id="page">
      <h1>
        <span style="color: red">Sharing</span>
        <span style="color: blue">is</span>
        <span style="color: green">Caring</span>
      </h1>

      <div id="up">
        <form action="/upload" method="post" enctype="multipart/form-data">
          <input id="file" name="file" type="file">
          <input type="submit" value="Hochladen">
        </form>
        <p class="hint">
          Wie wärs mit einem schicken Foto? Dem Flyer der nächsten
          Party? Oder gar einem gesprochenen Kommentar?
        </p>
      </div>
)HASENBLUBBE" ;


static const std::string std_footer= u8R"FISCHBLUBBE(
      <p class="foot">
	Eine Guerilla-Kunstaktion von <b>&lt;&lt;&lt;/&gt;&gt;</b>
      </p>
    </div>
  </body>
</html>
)FISCHBLUBBE" ;


Routerin::Routerin():
	sfh( new StaticFileHandler())
{
	cout << "Konstr Routering" << endl;
}

Routerin::~Routerin()
{
	delete _dyn_obs;
}

void Routerin::start()
{
	_dyn_obs = new DynamicObjects(this);
}


int event_route_wrap(struct mg_connection *conn, mg_event ev)
{
	Routerin *rouri = Routerin::get_instance();
	return	rouri->event_route(conn,ev);
}


int Routerin::event_request_route(struct mg_connection *conn){
	const RouterOp *blub=0;
	Foohash fh;
	const char *reqstr=0;
	char *tmpstr = 0,*pntrstr=0;
	int retval=-1;
	reqstr=conn->uri;
	if (reqstr){
		tmpstr = strdup(reqstr);
		std::cerr  << "starting with str as "
			<< tmpstr << endl; //DEBUG CODE
		pntrstr = strchrnul((tmpstr+1),'/');
		if (pntrstr) {
			*pntrstr = 0;
			std::cerr <<" having reqstr as " << tmpstr 
				<< " now. " << endl;
			std::cerr <<"len of reqstr is " <<
				( pntrstr - tmpstr) << endl;
			blub = fh.in_word_set(tmpstr,((pntrstr++)-tmpstr) );
			if (blub){
				std::cerr << "me has blub!!" <<endl;
				switch(blub->op){
					case RouterOpCode::FILES:
						retval = rq_file(pntrstr ,conn);
						break;
					case RouterOpCode::DATE:
						retval = rq_date(pntrstr, conn);
						break;
					case RouterOpCode::UPLOAD:
						retval = rq_upload(pntrstr, conn);
						break;
					case RouterOpCode::STATIC:
						std::cerr << "(/static/ given)";
						retval = rq_static((pntrstr),conn);
						break;
					default: 
						std::cerr << "fallback ";
						retval =  rq_static("illegalurl.htm",conn); 
				};
			}else{
				std::cerr << "no blub! in " << __LINE__ << endl;
				//retval =  rq_static("illegalurl.htm",event); 
				retval = rq_render_mainpage(conn);

			}
		}
		std::free(tmpstr);
		tmpstr=0;
		return retval;
	} else {
		std::cerr << "event request without uri? ?? hu? [" <<
			conn->uri << "]" << endl;
	}
	return 0;
}


void Routerin::set_baseurl(std::string baseurl)
{
	this->baseurl = baseurl;
	return;
}
void Routerin::set_file_path(std::string file_path)
{
	this->file_path = file_path;
	return;
}

void Routerin::set_json_path(std::string json_path)
{
	this->json_path = json_path;
	return;
}



int Routerin::rq_render_mainpage(struct mg_connection *conn)
{
	std::cerr << "rendering main page!" << endl;
	const std::string fubb = 
		std_html_header +
		std_mainpage +
		_dyn_obs->render_object_list()+
		std_footer.c_str() ;

	mg_printf(conn, "HTTP/1.0 200 OK\r\n"
			"Content-Length: %lu\r\n"
			"Content-Type: text/html\r\n\r\n%s",
			fubb.size(),
			fubb.c_str());

	return 1;
}



int Routerin::rq_static(char const * const reststr , struct mg_connection *conn)
{
	std::cerr << " a static file is reqested!" << endl;
	std::cerr << " path: " << reststr << endl;
	return sfh->answer_pathreq(reststr,conn);
}



int Routerin::rq_file(char const * const reststr,
		struct mg_connection *conn __attribute__((unused)))
{
	std::cerr << " a dynamic file is reqested!" << endl;
	std::cerr << " path: " << reststr << endl;
	return sfh->answer_pathreq(reststr,conn);
}

int Routerin::rq_date(char const * const reststr   __attribute__((unused)),
		struct mg_connection *conn __attribute__((unused))  )
{
	std::cerr << "requested filelist ordered by date!" << endl ;
	return 1;
}

int Routerin::rq_upload(char const * const reststr __attribute__((unused)),
		struct mg_connection *conn  __attribute__((unused)))
{
	std::cerr << "here we will get uploadish!" << endl;
 	std::cerr << "handle: " << handle_upload( conn ) << "fup!" << endl;

	// get Dynamic Object form Upload
	// Persist
	// render thankyou page
	return MG_TRUE;
}

/*int Routerin::event_requend_route(struct mg_connection *conn __attribute__((unused)) ){*/
	//std::cerr << "ended request" <<endl;
	//return 1;
/*}*/

int Routerin::event_fallback_route(enum mg_event event, struct mg_connection *conn __attribute__((unused)) ){
	//DEBUG CODE
	std::cerr << "Got an unhandled mg_event" << endl;
	std::cerr << "Its type is: " << event << endl;
	return 1;
}


//int Routerin::event_newthread_route(struct mg_connection *conn __attribute__((unused)) ){
	////DEBUG CODE
	//std::cerr << "Started new thread!" <<endl;
	//return 1;
//}


int Routerin::handle_upload(struct mg_connection *conn){
	const char *data;
	int data_len;
	int ret;
	char var_name[100], file_name[100];
	ret = mg_parse_multipart(conn->content, conn->content_len,
			var_name, sizeof(var_name),
			file_name, sizeof(file_name),
			&data, &data_len);
	if (ret > 0) {
		std::cerr << " got " << file_name << "with len of " << data_len << endl;
	}
	return ret;
}


/*
 * 
static int event_handler(struct mg_event *event) {

	if (event->type == MG_REQUEST_BEGIN) {
		if (!strcmp(
		      event->request_info->uri,
		      "/handle_post_request")) {
			char path[200];
			FILE *fp = mg_upload(event->conn,
					"/tmp",
					path,
					sizeof(path));
			if (fp != NULL) {
				fclose(fp);
				mg_printf(event->conn,
					"HTTP/1.0 200 OK\r\n\r\nSaved: [%s]",
					path);
			} else {
				mg_printf(event->conn,"%s",
				  "HTTP/1.0 200 OK\r\n\r\nNo files sent");
			}
		} else {
			// Show HTML form. Make sure it has enctype="multipart/form-data" attr.
			static const char *html_form =
				"<html><body>Upload example."
				"<form method=\"POST\" action=\"/handle_post_request\" "
				"  enctype=\"multipart/form-data\">"
				"<input type=\"file\" name=\"file\" /> <br/>"
				"<input type=\"submit\" value=\"Upload\" />"
				"</form></body></html>";

			mg_printf(event->conn, "HTTP/1.0 200 OK\r\n"
					"Content-Length: %d\r\n"
					"Content-Type: text/html\r\n\r\n%s",
					(int) strlen(html_form), html_form);
		}

		// Mark request as processed
		return 1;
	}

	// All other events left unprocessed
	return 1;
}
*/


//REST API Documentation

///files/<zahl> (index in vector<Sicccobjst>)

///date/<datestring> (files am date hochgeladen)

///static/<mööp.html> (statisches html ausliefern)

///uploadmagic/        (upload form request)


