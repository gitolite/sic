#include "DynamicObjects.H"

//add parameter for ordering??
std::string DynamicObjects::render_object_list()
{
	std::string output = u8R"FOO(
<ul id="down">
)FOO";

	for (auto& it : this->m_thevec){
		output += u8"<li>\n";
		output += u8"<a href=\"" + m_merouterin->get_baseurl() + u8"files/"+ it.get_filename() + u8"\">"; //we could include baseurl here?
		output += it.get_filename();
		output += u8"</a> <span class=\"size\">";
		output += it.get_pretty_sized();
		output += u8"</span>\n</li>";
	}
	output += u8R"HUENERBLUBBE(
</ul>
)HUENERBLUBBE";
	return output;
}
        //<li>
          //<a href="/file/23/Foobar.jpg">Foobar.jpg</a> <span class="size">80 KB</span>
          //<span class="stats"><b>42×</b> seit 1.1.1970 0:00 Uhr</span>   //<--- TODO
        //</li>
	//

DynamicObjects::DynamicObjects(Routerin * merouterin) :
	m_merouterin(merouterin)
{
    m_thevec = SicccPersister::read_sicccdir(*merouterin);
}


DynamicObjects::~DynamicObjects()
{

}

