#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <iostream>
#include <signal.h>

#include <mongoose.h>

#include "options.h"
#include "Routerin.H"
//globally accessible config
static struct sic_conf args_info;

using std::cout;


class MongooseHandler{
	private:
		struct mg_context *ctx;
		//const char *options[];
		unsigned short listenport;
		//	= {"listening_ports", "8080", NULL};
		//
		struct mg_server * srv;

	public:

		//default listenport:8080
		MongooseHandler(int listenport=8080):
			listenport(listenport),
			srv(nullptr)
		{
		char portstring[8];
		mg_handler_t mh = & event_route_wrap;
		srv = mg_create_server(NULL, mh);
		std::snprintf(portstring,8,"%d",this->listenport);
		mg_set_option(srv,"listening_port",portstring);
		//this->ctx= mg_start(options, Routerin::event_route, nullptr);

		}


		void serveloop(const int milisectimeout){
			assert(srv);
			mg_poll_server(srv,milisectimeout);
			return;
		}
		
		//no copy constructor:
		MongooseHandler( const MongooseHandler &) = delete;

		//destructor stops the server
		~MongooseHandler(){
			if (this->srv){
				mg_destroy_server(&(this->srv));
			}
		}

};

#ifdef DEBUG
void dump_args()
{
	cout <<  "value of port: " << args_info.port_arg << std::endl;
	cout << "value of daemonize: " <<
		static_cast<bool>(args_info.daemonize_flag) <<
		std::endl;

	cout << "value of listen_given: "<<
		args_info.listen_given << std::endl ;
	for (unsigned int i = 0; i < args_info.listen_given; i++)
		cout << "value of listen: " <<
			args_info.listen_arg[i] <<std::endl;


	// if (args_info.saveconf_given) {
	// 	if (cmd_parser_file_save(args_info.conffile_arg,
	// 				&args_info) == EXIT_FAILURE)
	// 		
	// 	else
	// 		cout << "saved configuration file "<<
	// 			args_info.conffile_arg <<
	// 			", \n" << std::endl;
	// }
    return;
}
#endif

bool configfile_parsing_action(int& argc, char **argv)
{
	struct cmd_parser_params *params;

	//initialize the parameters structure
	params = cmd_parser_params_create();

    params->check_required = 0;

	if (cmd_parser_config_file("./siccc.conf", &args_info, params) != 0) {
        cmd_parser_free(&args_info);
        free(params);
        return false;
	}

	params->initialize = 0;
	params->override = 1;
    params->check_required = 1;

	//call the command line parser
	if (cmd_parser(argc, argv, &args_info) != 0) {
        cmd_parser_free(&args_info);
        free(params);
        return false;
	}
    return true;
}

bool killme;

static void signalhandler(int signum){
	switch(signum){
		case SIGTERM:
		case SIGINT:
			killme=true;
			break;
		default:
			std::cout << "called sighandler with signal " << signum << " FUPP! " << std::endl;
	}
}


auto main(int argc, char **argv)->int
{
	killme = false;
    if ( !configfile_parsing_action(argc, argv) ) {
        std::cerr << "ERROR ERROR BEEP" << std::endl;
        exit(1);
    }


#ifdef DEBUG
    dump_args();
    if (std::atexit(
	[](){std::cout << "alles hat ein ende" << std::endl;}
	))
	    exit(-4);
#endif
    struct sigaction sa;
    sa.sa_handler = signalhandler;
    sigaction(SIGINT, &sa ,nullptr);
    sigaction(SIGTERM, &sa ,nullptr);

    Routerin * merouterin = Routerin::get_instance();

    merouterin->set_baseurl(args_info.baseurl_arg);
    merouterin->set_json_path(args_info.janssons_arg);
    merouterin->set_file_path(args_info.filedir_arg);

    merouterin->start();

    std::cerr << "baseurl: "<< merouterin->get_baseurl() << "\n";
    std::cerr << "json_path: "<< merouterin->get_json_path() << "\n";
    std::cerr << "file_path: "<< merouterin->get_file_path() << "\n";

    MongooseHandler *m = new MongooseHandler(args_info.port_arg);

    if (!args_info.daemonize_flag) {
	    while((not killme))
	    		m->serveloop(1000);
    }
    delete m;

    return 0;
}
