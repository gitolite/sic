#include "sicccobject.H"
#include "sicccPersister.H"


// FIXME:
// check for right reference counting of val
Sicccobject::Sicccobject(const json_t *j)
{
    assert(j);
    json_t *val;
    val = json_object_get(j, "filename");
    filename = SicccPersister::get_string_from_json(val);
    val = json_object_get(j, "filetype");
    filetype = SicccPersister::get_string_from_json(val);
    val = json_object_get(j, "size");
    size = SicccPersister::get_integer_from_json(val);
    val = json_object_get(j, "upload_time");
    upload_date = SicccPersister::get_time_from_json(val);
    json_decref(val);
}

Sicccobject::Sicccobject(const Sicccobject& obj)
{
    filename = obj.filename;
    filetype = obj.filetype;
    size = obj.size;
    upload_date = obj.upload_date;
}

Sicccobject&
Sicccobject::operator = (const Sicccobject& obj)
{
    if (this == &obj)
        return *this;

    this->filename = obj.filename;
    this->filetype = obj.filetype;
    this->size = obj.size;
    this->upload_date = obj.upload_date;

    return *this;
}

std::ostream&
operator << (std::ostream& out, const Sicccobject& obj)
{
    std::time_t time =
        std::chrono::system_clock::to_time_t(obj.get_uploadtime());
    out << "[Filename] " << obj.get_filename() << std::endl
        << "[Filetype] " << obj.get_filetype() << std::endl
        << "[Size] " << obj.get_size() << std::endl
        << "[Uploaded] " << std::ctime(&time) << std::endl;
    return out;
}
